(function($) {
Drupal.behaviors.simplifiedPermissions = {
  attach: function (context, settings) {
    $('.check-all').once('check-all', function(){      
      var checkboxes = $(this).parent().find('input[type="checkbox"]');
      new Drupal.simplifiedPermissions(this, checkboxes);
    });
  }
};

Drupal.simplifiedPermissions = function (trigger, checkboxes) {
  var master = this;
  master.checkall = true;
  $.each(checkboxes, function(i, v){
    if (v.checked === false) {
      master.checkall = false;
    }
  });
  
  master.checkboxes = checkboxes;  
  $(trigger).click(function(event){
    master.onclick();
    event.preventDefault();
  });  
}
Drupal.simplifiedPermissions.prototype.onclick = function () {  
  var master = this;
  if (master.checkall) {
    $.each(master.checkboxes, function(i, v){
      v.checked = false;
    });
    master.checkall = false;
  }
  else {
    $.each(master.checkboxes, function(i, v){
      v.checked = true;
    });
    master.checkall = true;
  }
}
})(jQuery);
